// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

//! Module for hashing strings

use std::num::Wrapping;

/// Calculates 32-bit binhash of the supplied string
pub fn binhash(s: &str) -> u32 {
    let mut i = Wrapping(u32::MAX);
    for ch in s.chars() {
        i *= Wrapping(33);
        i += Wrapping(ch as u32);
    }
    i.0
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_binhash() {
        assert_eq!(binhash("TestTestTest"), 0xF029_7CBF);
    }
}