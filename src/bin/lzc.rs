// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use libworld::lzc;
use libworld::lzc::FileType;
use std::env;
use std::fs;

type AnyResult = Result<(), Box<dyn std::error::Error>>;

fn main() -> AnyResult {
    let args: Vec<String> = env::args().collect();
    if args.len() < 3 {
        return Err("At least 2 arguments expected".into())
    }
    match args[1].as_str() {
        "unpack" => {
            let dest = if args.len() > 3 {
                args[3].clone()
            } else {
                args[2].clone() + ".bin"
            };
            unpack(&args[2], &dest)
        },
        _ => Err("Unknown command".into())
    }
    
}

fn unpack(file: &str, out: &str) -> AnyResult {
    let data = fs::read(file)?;
    let uncompressed = match lzc::detect_type(&data) {
        FileType::LZC => {
            let file = lzc::parse_file(&data)?;
            file.uncompress()?
        },
        FileType::LZCHeaderless => {
            let chunk = lzc::parse_chunk(&data)?;
            chunk.uncompress()?
        },
        FileType::Unknown => {
            return Err("Unknown file type".into())
        }
    };
    fs::write(out, uncompressed)?;
    Ok(())
}
