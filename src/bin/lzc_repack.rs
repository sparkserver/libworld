// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use libworld::lzc;
use std::env;
use std::fs;
use std::fs::File;
use libworld::compression::DefaultCompressor;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let filename = env::args().nth(1).unwrap();
    let data = fs::read(&filename)?;

    let lzc_in_file = lzc::parse_file(&data)?;
    let uncomp_data = lzc_in_file.uncompress()?;

    let lzc_out_file = lzc::LZCFile::from_data::<DefaultCompressor>(&uncomp_data);

    let mut out_file = File::create(&filename)?;
    lzc_out_file.write_to(&mut out_file)?;
    Ok(())
}