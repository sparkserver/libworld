// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use libworld::lzc;
use std::env;
use std::fs;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let in_file = env::args().nth(1).unwrap();
    let data = fs::read(in_file)?;
    let file = lzc::parse_file(&data)?;
    println!("- File");
    println!("\tuncompressed_size={:#x}", file.uncompressed_size);
    for (i, chunk) in file.chunks.iter().enumerate() {
        println!("- Chunk {}", i);
        println!("\tuncompressed_size={:#x}", chunk.uncompressed_size);
        println!("\toffset={:#x}", chunk.offset);
    }
    Ok(())
}