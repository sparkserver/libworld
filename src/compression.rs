// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use thiserror::Error;

pub mod jdlz;

#[cfg(windows)]
pub mod native;

#[derive(Error, Debug, PartialEq)]
pub enum CompressionError {
    #[error("unknown algorithm: {0:02x?}")]
    UnknownAlgorithm([u8; 4]),
    #[error("not enough data")]
    NotEnoughData,
    #[error("invalid data")]
    InvalidData,
    #[error("unknown error")]
    UnknownError,
    #[error("output too small")]
    OutputTooSmall
}

pub type CompressionResult<T> = Result<T, CompressionError>;

pub trait Compress {
    /// Compresses the supplied input buffer
    fn compress(data: &[u8]) -> Vec<u8>;
}

pub trait Decompress {
    /// Decompresses the supplied input buffer to the supplied output buffer
    fn decompress(data: &[u8], output: &mut [u8]) -> CompressionResult<usize>;
}

/// The default compressor
/// 
/// At the moment JDLZ algorithm is used.
#[cfg(windows)]
pub type DefaultCompressor = jdlz::Compressor;

/// Tries to automatically choose an appropriate decompressor
pub struct AutoDecompressor;

impl Decompress for AutoDecompressor {
    fn decompress(data: &[u8], output: &mut [u8]) -> CompressionResult<usize> {
        if data.len() < 4 {
            return Err(CompressionError::NotEnoughData);
        }

        match &data[0..4] {
            b"JDLZ" => jdlz::Decompressor::decompress(data, output),

            #[cfg(windows)]
            _ => native::Decompressor::decompress(data, output),

            #[cfg(not(windows))]
            _ => Err(CompressionError::UnknownAlgorithm([
                input[0], input[1], input[2], input[3]
            ])),
        }
    }
}
