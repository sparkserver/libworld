// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include <stdexcept>
#include <cstring>

extern "C" {
    size_t JLZCompress(const char *input, size_t input_len, char *output);
    size_t LZDecompress(const char *input, size_t input_len, char *output, size_t output_len);

    size_t native_decompress(const char *input, size_t input_len, char *output, size_t output_len, int *error) {
        *error = 0;

        size_t ret;
        try {
            ret = LZDecompress(input, input_len, output, output_len);
        } catch (const std::runtime_error& e) {
            if (std::strcmp(e.what(), "Not enough input data!") == 0) {
                *error = 2;
            } else if (std::strcmp(e.what(), "Unknown compression type!") == 0) {
                *error = 3;
            } else {
                *error = 1;
            }
        }
        return ret;
    }
}