// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use crate::compression::{Decompress, Compress, CompressionError, CompressionResult};

#[link(name = "native")]
extern {
    fn JLZCompress(input: *const u8, input_len: usize, output: *mut u8) -> usize;
    fn native_decompress(input: *const u8, input_len: usize, output: *mut u8, output_len: usize, error: *mut u32) -> usize;
}

pub struct JDLZCompressor;

impl Compress for JDLZCompressor {
    fn compress(data: &[u8]) -> Vec<u8> {
        let max_out = data.len() + (data.len() + 7) / 8 + 16 + 1;
        let mut vec = vec![0; max_out];

        let len = unsafe { JLZCompress(data.as_ptr(), data.len(), vec.as_mut_ptr()) };
        vec.resize(len, 0);

        vec
    }
}

pub struct Decompressor;

impl Decompress for Decompressor {
    fn decompress(input: &[u8], output: &mut [u8]) -> CompressionResult<usize> {
        let mut error = 0u32;
        let len = unsafe {
            native_decompress(input.as_ptr(), input.len(), output.as_mut_ptr(), output.len(), &mut error)
        };
        match error {
            0 => Ok(len),
            2 => Err(CompressionError::NotEnoughData),
            3 => Err(CompressionError::UnknownAlgorithm([input[0], input[1], input[2], input[3]])),
            _ => Err(CompressionError::UnknownError),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::compression::{Decompress, CompressionError};
    use super::Decompressor;

    #[test]
    fn decompress_not_enough_data() {
        let res = Decompressor::decompress(&[0x00], &mut []);
        assert_eq!(res, Err(CompressionError::NotEnoughData));
    }

    #[test]
    fn decompress_unknown_algorithm() {
        let algo = [0x12, 0x34, 0x56, 0x78];
        let mut input = vec![0u8; 16];
        input[0..4].clone_from_slice(&algo);
        let res = Decompressor::decompress(&input, &mut []);
        assert_eq!(res, Err(CompressionError::UnknownAlgorithm(algo)));
    }
}
