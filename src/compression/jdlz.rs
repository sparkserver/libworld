// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

mod decompress;

#[cfg(windows)]
pub use super::native::JDLZCompressor as Compressor;

pub use decompress::Decompressor;

#[cfg(test)]
mod tests {
    use std::time::Instant;
    use crate::compression::{Decompress, Compress};
    use super::*;

    #[test]
    fn compress_decompress() {
        let data = include_bytes!("shakespeare.txt");

        let start = Instant::now();
        let compressed = Compressor::compress(data);
        println!("Compression done in {:?} : ratio {:.02}% ", start.elapsed(), compressed.len() as f64 / data.len() as f64 * 100.0);

        let mut decompressed = vec![0u8; data.len()];
        Decompressor::decompress(&compressed, &mut decompressed).unwrap();
        assert!(data.iter().eq(decompressed.iter()), "decompressed data doesn't match original");
    }
}