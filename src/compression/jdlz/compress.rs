// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

// Slow broken code, either fix it or stay away

use byteorder::{LE, ByteOrder};

pub fn compress(data: &[u8]) -> Vec<u8> {
    let mut vec = b"JDLZ\x02\x10\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00".to_vec();
    let mut c = JDLZCompressor{
        rd_pos: 0,
        rd: data,
        wr: &mut vec,

        gflag_off: 0,
        gflag_cnt: 8,
        bflag_off: 0,
        bflag_cnt: 8,
    };
    while c.rd_pos < data.len() {
        c.step();
    }

    LE::write_u32(&mut vec[8..12], data.len() as u32);
    let len = vec.len() as u32;
    LE::write_u32(&mut vec[12..16], len);

    vec
}

struct JDLZCompressor<'a> {
    rd_pos: usize,
    rd: &'a [u8],
    wr: &'a mut Vec<u8>,

    gflag_off: usize,
    gflag_cnt: u8,
    bflag_off: usize,
    bflag_cnt: u8
}

impl JDLZCompressor<'_> {
    fn push_gflag(&mut self, val: bool) {
        let i = if val { 1 } else { 0 };
        self.wr[self.gflag_off] |= i << self.gflag_cnt;
        self.gflag_cnt += 1;
    }

    fn push_bflag(&mut self, val: bool) {
        let i = if val { 1 } else { 0 };
        self.wr[self.bflag_off] |= i << self.bflag_cnt;
        self.bflag_cnt += 1;
    }

    fn try_backtrack(&self, offset: usize, max: usize) -> usize {
        for i in 0..max {
            if self.rd_pos + i >= self.rd.len() {
                return i
            }
            if self.rd[self.rd_pos + i - offset] != self.rd[self.rd_pos + i] {
                return i
            }
        }
        max
    }

    fn write_backtrack1(&mut self, offset: u16, len: u16) {
        let plen = len - 3;
        let poff = offset - 1;
        self.wr.push(((plen >> 4 & 0xF0) | poff) as u8);
        self.wr.push(plen as u8);
    }

    fn write_backtrack2(&mut self, offset: u16, len: u16) {
        let plen = len - 3;
        let poff = offset - 17;
        self.wr.push(((poff >> 5 & 0xE0) | plen) as u8);
        self.wr.push(poff as u8);
    }

    fn step(&mut self) {
        if self.gflag_cnt == 8 {
            self.gflag_off = self.wr.len();
            self.gflag_cnt = 0;
            self.wr.push(0x00);
        }
        if self.bflag_cnt == 8 {
            self.bflag_off = self.wr.len();
            self.bflag_cnt = 0;
            self.wr.push(0x00);
        }

        let mut best_bt = (false, 0, 0);
        if self.rd_pos >= 1 {
            for i in 1..=self.rd_pos.min(16) {
                let res = self.try_backtrack(i, 4098);
                if best_bt.2 < res {
                    best_bt = (true, i, res);
                }
            }
        }
        if self.rd_pos >= 17 && best_bt.2 < 34 {
            for i in 17..=self.rd_pos.min(2064) {
                let res = self.try_backtrack(i, 34);
                if best_bt.2 < res {
                    best_bt = (false, i, res);
                }
            }
        }

        if best_bt.2 >= 3 {
            // println!("at {} doing backtrack: {:?}", self.rd_pos, best_bt);
            self.push_gflag(true);
            self.push_bflag(best_bt.0);
            if best_bt.0 {
                self.write_backtrack1(best_bt.1 as u16, best_bt.2 as u16);
            } else {
                self.write_backtrack2(best_bt.1 as u16, best_bt.2 as u16);
            }
            self.rd_pos += best_bt.2;
        } else {
            // println!("at {} passing byte", self.rd_pos);
            self.wr.push(self.rd[self.rd_pos]);
            self.rd_pos += 1;
            self.push_gflag(false);
        }
    }
}