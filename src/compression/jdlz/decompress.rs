// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use crate::compression::{Decompress, CompressionError, CompressionResult};
use byteorder::{ByteOrder, LE};

pub struct Decompressor;

impl Decompress for Decompressor {
    fn decompress(data: &[u8], out: &mut [u8]) -> CompressionResult<usize> {
        if data.len() < 16 {
            return Err(CompressionError::NotEnoughData);
        }
        let magic = &data[0..5];
        if magic != [0x4A, 0x44, 0x4C, 0x5A, 0x02] {
            return Err(CompressionError::InvalidData);
        }
        let uncomp_len = LE::read_u32(&data[8..12]) as usize;
        let data_len = LE::read_u32(&data[12..16]) as usize;
        if data.len() < data_len {
            return Err(CompressionError::NotEnoughData);
        }
        if out.len() < uncomp_len {
            return Err(CompressionError::OutputTooSmall);
        }
        
        let len = decompress_internal(&data[16..data_len], out);

        Ok(len)
    }
}

fn decompress_internal(buf: &[u8], out: &mut [u8]) -> usize {
    let mut flags1 = 1u16;
    let mut flags2 = 1u16;
    let mut idx = 0usize;
    let mut out_idx = 0usize;
    let mut len;
    let mut t;

    while idx < buf.len() && out_idx < out.len() {
        if flags1 == 1 {
            flags1 = (buf[idx] as u16) | 0x100;
            idx += 1;
        }
        if flags2 == 1 {
            flags2 = (buf[idx] as u16) | 0x100;
            idx += 1;
        }
        if flags1 & 1 == 1 {
            if flags2 & 1 == 1 {
                len = (buf[idx+1] as usize | ((buf[idx] as usize & 0xF0) << 4)) + 3;
                t = (buf[idx] as usize & 0x0F) + 1;
            } else {
                t = (buf[idx+1] as usize | ((buf[idx] as usize & 0xE0) << 3)) + 17;
                len = (buf[idx] as usize & 0x1F) + 3;
            };
            idx += 2;
            for i in 0..len {
                out[out_idx+i] = out[out_idx+i-t];
            }
            out_idx += len;
            flags2 >>= 1;
        } else {
            out[out_idx] = buf[idx];
            out_idx += 1;
            idx += 1;
        }
        flags1 >>= 1;
    }

    out_idx
}
