// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

//! Module for working with LZC files
//! 
//! LZC files consist of one or more compressed chunks, that when uncompressed 
//! and combined, form a single binary file.

use std::fmt;
use thiserror::Error;
use byteorder::{LE, ByteOrder, WriteBytesExt};
use crate::compression::{
    AutoDecompressor, Compress, Decompress,
    CompressionError, CompressionResult
};
use std::borrow::Cow;
use std::collections::VecDeque;
use std::io::Result as IOResult;
use std::io::Write;

const LZC_FILE_MAGIC: &[u8] = &[0x88, 0x33, 0x11, 0x66];
const LZC_CHUNK_MAGIC: &[u8] = &[0x22, 0x11, 0x44, 0x55];

const LZC_CHUNK_SIZE: usize = 0x8000;

#[derive(Error, Debug, PartialEq)]
pub enum LZCError {
    #[error("not enough data")]
    NotEnoughData,
    #[error("invalid data")]
    InvalidData
}

pub type LZCResult<T> = Result<T, LZCError>;

/// Represents the detected file type
#[derive(Debug, PartialEq)]
pub enum FileType {
    /// LZC file with a header and one or more chunks
    LZC,
    /// LZC file without header and with only one chunk
    LZCHeaderless,
    /// Not a proper LZC file
    Unknown
}

impl fmt::Display for FileType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::LZC => write!(f, "LZC"),
            Self::LZCHeaderless => write!(f, "LZC (headerless)"),
            Self::Unknown => write!(f, "Unknown"),
        }
    }
}

/// Detects the type of the supplied buffer
pub fn detect_type(data: &[u8]) -> FileType {
    if data.len() < 4 {
        return FileType::Unknown;
    }

    match &data[0..4] {
        LZC_FILE_MAGIC => FileType::LZC,
        LZC_CHUNK_MAGIC => FileType::LZCHeaderless,
        _ => FileType::Unknown
    }
}

/// Represents a LZC file
pub struct LZCFile<'a> {
    /// The uncompressed size of the chunk
    pub uncompressed_size: u32,
    /// Chunks contained in the file
    pub chunks: Vec<LZCChunk<'a>>
}

impl LZCFile<'_> {
    /// Creates a [`LZCFile`] from the supplied buffer
    pub fn from_data<C: Compress>(data: &[u8]) -> LZCFile {
        let mut chunks = VecDeque::new();
        let mut offset = 0;
        while offset < data.len() {
            let end_offset = usize::min(offset+LZC_CHUNK_SIZE, data.len());
            let payload = C::compress(&data[offset..end_offset]);
            chunks.push_back(LZCChunk{
                uncompressed_size: (end_offset - offset) as u32,
                offset: offset as u32,
                payload: Cow::Owned(payload),
            });
            offset += LZC_CHUNK_SIZE;
        }

        // Moves the first chunk to last in the file
        // Not sure if it's actually neccessary, but it's like that in the 
        // official game files
        chunks.rotate_left(1);

        LZCFile{
            uncompressed_size: data.len() as u32,
            chunks: chunks.into()
        }
    }

    /// Uncompresses the entire file
    pub fn uncompress(&self) -> CompressionResult<Vec<u8>> {
        let mut buf = vec![0; self.uncompressed_size as usize];
        for chunk in self.chunks.iter() {
            let off = chunk.offset as usize;
            let len = chunk.uncompressed_size as usize;
            chunk.uncompress_to(&mut buf[off..off+len])?;
        }
        Ok(buf)
    }

    /// Writes the file to a writer
    pub fn write_to<W: Write>(&self, wr: &mut W) -> IOResult<()> {
        wr.write_all(LZC_FILE_MAGIC)?;
        wr.write_u32::<LE>(self.uncompressed_size)?;
        let compressed_len = self.chunks.iter()
            .fold(0, |acc, x| acc + 24 + x.payload.len());
        wr.write_u32::<LE>(compressed_len as u32)?;
        // For some reason GlobalC has this set to 0x00???
        // From testing it seems that the value doesn't really matter, so let's
        // keep it 0x02, as majority of files use that value
        wr.write_u32::<LE>(0x02)?;

        for chunk in self.chunks.iter() {
            chunk.write_to(wr)?;
        }

        Ok(())
    }
}

/// Represents a LZC chunk
pub struct LZCChunk<'a> {
    /// The uncompressed size of the chunk
    pub uncompressed_size: u32,
    /// Offset for the chunk's payload in the uncompressed file
    pub offset: u32,
    /// Compressed payload
    pub payload: Cow<'a, [u8]>
}

impl LZCChunk<'_> {
    /// Uncompresses the chunk
    pub fn uncompress(&self) -> CompressionResult<Vec<u8>> {
        let mut buf = vec![0u8; self.uncompressed_size as usize];
        self.uncompress_to(&mut buf)?;
        Ok(buf)
    }

    /// Uncompresses the chunk into the supplied buffer.
    /// Offset is ignored - it's the caller's responsibility to supply 
    /// destination slice with correct offset.
    pub fn uncompress_to(&self, dest: &mut [u8]) -> CompressionResult<()> {
        if dest.len() < self.uncompressed_size as usize {
            return Err(CompressionError::OutputTooSmall);
        }
        AutoDecompressor::decompress(&self.payload, dest)?;
        Ok(())
    }

    /// Writes the chunk to a writer
    pub fn write_to<W: Write>(&self, wr: &mut W) -> IOResult<()> {
        wr.write_all(LZC_CHUNK_MAGIC)?;
        wr.write_u32::<LE>(self.uncompressed_size)?;
        wr.write_u32::<LE>(self.payload.len() as u32 + 24)?;
        wr.write_u32::<LE>(self.offset)?;
        wr.write_all(&[0; 8])?;
        wr.write_all(&self.payload)?;
        Ok(())
    }
}

/// Parses a LZC file
pub fn parse_file(data: &[u8]) -> LZCResult<LZCFile> {
    if data.len() < 16 {
        return Err(LZCError::NotEnoughData);
    }

    let magic = &data[0..4];
    if magic != LZC_FILE_MAGIC {
        return Err(LZCError::InvalidData);
    }
    let uncomp_size = LE::read_u32(&data[4..8]);
    let comp_size = LE::read_u32(&data[8..12]);

    if data.len() < (comp_size + 16) as usize {
        return Err(LZCError::NotEnoughData);
    }

    let mut chunks = Vec::new();
    let mut offset = 16;
    while offset < comp_size as usize {
        let chunk = parse_chunk(&data[offset..])?;
        offset += chunk.payload.len() + 24;
        chunks.push(chunk);
    }

    Ok(LZCFile{
        uncompressed_size: uncomp_size,
        chunks,
    })
}

/// Parses a single LZC chunk
pub fn parse_chunk(data: &[u8]) -> LZCResult<LZCChunk> {
    if data.len() < 24 {
        return Err(LZCError::NotEnoughData);
    }

    let magic = &data[0..4];
    if magic != LZC_CHUNK_MAGIC {
        return Err(LZCError::InvalidData);
    }
    let uncomp_size = LE::read_u32(&data[4..8]);
    let comp_size = LE::read_u32(&data[8..12]);
    let offset = LE::read_u32(&data[12..16]);

    if data.len() < comp_size as usize {
        return Err(LZCError::NotEnoughData);
    }

    Ok(LZCChunk{
        uncompressed_size: uncomp_size,
        offset,
        payload: Cow::Borrowed(&data[24..comp_size as usize]),
    })
}